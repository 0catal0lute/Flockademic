import { periodicalNameValidators, periodicalSlugValidators } from '../../validation/periodical';

describe('Periodical name', () => {
  it('should not be empty', () => {
    expect(periodicalNameValidators.map((validator) => validator.id).indexOf('minLength')).not.toBe(-1);
    expect(periodicalNameValidators.find((validator) => validator.id === 'minLength').options)
      .toEqual({ minLength: 1 });
  });
});

describe('Periodical slug', () => {
  it('should not be empty', () => {
    expect(periodicalSlugValidators.map((validator) => validator.id).indexOf('minLength')).toBeDefined();
    expect(periodicalSlugValidators.find((validator) => validator.id === 'minLength').options)
      .toEqual({ minLength: 1 });
  });

  it('should not be as long as a UUID', () => {
    expect(periodicalSlugValidators.map((validator) => validator.id).indexOf('maxLength')).toBeDefined();
    expect(periodicalSlugValidators.find((validator) => validator.id === 'maxLength').options)
      .toEqual({ maxLength: 35 });
  });
});
