import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import * as React from 'react';

const mockProfiles = [
  { name: 'arbitrary name', orcid: 'https://orcid.org/arbitrary-orcid' },
  { name: 'arbitrary name', orcid: 'https://orcid.org/arbitrary-orcid' },
  { name: 'arbitrary name', orcid: 'https://orcid.org/arbitrary-orcid' },
];

jest.mock('react-ga');
jest.mock('../../src/services/orcid', () => ({
  searchProfiles: jest.fn().mockReturnValue(Promise.resolve(mockProfiles)),
}));

import { ProfileList } from '../../src/components/profileList/component';
import { ProfileSearch } from '../../src/components/profileSearch/component';

it('should render an error when there was an error searching for profiles', (done) => {
  const mockOrcidService = require.requireMock('../../src/services/orcid');
  mockOrcidService.searchProfiles.mockReturnValueOnce(Promise.reject('Arbitrary error'));

  const component = shallow(<ProfileSearch/>);
  component.find('input[type="search"]').simulate('change', { target: { value: 'Some search term' } });
  component.find('form').simulate('submit', { preventDefault: jest.fn() });

  setImmediate(() => {
    component.update();

    expect(mockOrcidService.searchProfiles.mock.calls.length).toBe(1);
    expect(mockOrcidService.searchProfiles.mock.calls[0][0]).toBe('Some search term');
    expect(component.find('.is-danger')).toExist();
    expect(component.find('.is-danger').text()).toBe('There was an error searching for researchers, please try again.');

    done();
  });
});

it('should render a warning when there no profiles were found', (done) => {
  const mockOrcidService = require.requireMock('../../src/services/orcid');
  mockOrcidService.searchProfiles.mockReturnValueOnce(Promise.resolve([]));

  const component = shallow(<ProfileSearch/>);
  component.find('input[type="search"]').simulate('change', { target: { value: 'Some search term' } });
  component.find('form').simulate('submit', { preventDefault: jest.fn() });

  setImmediate(() => {
    component.update();

    expect(mockOrcidService.searchProfiles.mock.calls.length).toBe(1);
    expect(mockOrcidService.searchProfiles.mock.calls[0][0]).toBe('Some search term');
    expect(component.find('.is-warning')).toExist();
    expect(component.find('.is-warning').text()).toBe('No researchers found — did you spell their name correctly?');

    done();
  });
});

it('should not render anything when no search term has been entered yet', () => {
  const component = shallow(<ProfileSearch/>);

  expect(component.find(ProfileList)).not.toExist();
});

it('should include a label for the search field', () => {
  const component = shallow(<ProfileSearch/>);

  expect(component.find('label')).toExist();
});

it('should mark the region where search results will appear for screen readers', () => {
  const component = shallow(<ProfileSearch/>);

  expect(component.find('[aria-live="polite"]')).toExist();
});

it('should display a spinner while searching', () => {
  const mockOrcidService = require.requireMock('../../src/services/orcid');
  mockOrcidService.searchProfiles.mockReturnValueOnce(new Promise(jest.fn()));

  const component = shallow(<ProfileSearch/>);
  component.find('input[type="search"]').simulate('change', { target: { value: 'Some search term' } });
  component.find('form').simulate('submit', { preventDefault: jest.fn() });

  expect(component.find('.is-loading')).toExist();
});

it('should send analytics events when searching', (done) => {
  const mockReactGa = require.requireMock('react-ga');

  const component = shallow(<ProfileSearch/>);
  component.find('input[type="search"]').simulate('change', { target: { value: 'Some search term' } });
  component.find('form').simulate('submit', { preventDefault: jest.fn() });

  setImmediate(() => {
    component.update();

    expect(mockReactGa.event.mock.calls.length).toBe(1);
    expect(mockReactGa.event.mock.calls[0][0]).toEqual({
      action: 'Search',
      category: 'Profile Search',
    });

    done();
  });
});

it('should display search results when available', (done) => {
  const mockOrcidService = require.requireMock('../../src/services/orcid');
  mockOrcidService.searchProfiles.mockReturnValueOnce(Promise.resolve(mockProfiles));

  const component = shallow(<ProfileSearch/>);
  component.find('input[type="search"]').simulate('change', { target: { value: 'Some search term' } });
  component.find('form').simulate('submit', { preventDefault: jest.fn() });

  setImmediate(() => {
    component.update();

    expect(mockOrcidService.searchProfiles.mock.calls.length).toBe(1);
    expect(mockOrcidService.searchProfiles.mock.calls[0][0]).toBe('Some search term');
    expect(component.find(ProfileList)).toExist();

    done();
  });
});
